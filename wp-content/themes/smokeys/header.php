<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta http-equiv="x-ua-compatible" content="IE=10">
		<meta name="viewport" content="width=1100, user-scalable=0">
        <title>Smokeys - All American BBQ</title>
        <link rel="stylesheet" href="<?php echo THEME_STYLES; ?>/main.css?<?php echo rand(0, 999999); ?>" type="text/css">
    </head>
    <body>

    	<header>
    		<div class="wrapper">
    			<h1>Smokeys - All American BBQ</h1>
    			<nav>
    				<ul>
    					<li><a href="#">Contact Us</a></li>
    					<li><a href="#">Locate Us</a></li>
    					<li><a href="#">Gallery</a></li>
    				</ul>
    			</nav>
    		</div>
    	</header>