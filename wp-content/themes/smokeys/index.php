<?php get_header(); ?>

	<section id="banner"></section>

	<section id="feature">
		<div class="wrapper">
			<div class="feature-item clearfix">
				<img class="pull-left" src="">
				<div class="media-body">
					<h2>Pulled Pork</h2>
					<p>Pulled pork is a form of barbecue in which shoulder pork is seasoned with a dry rub and cooked 'low and slow' for 18 hours.</p>
				</div>
			</div>
		</div>
	</section>

	<section id="secbanner">

	</section>

	<section id="statement">
		<div class="wrapper">
			<h3>"Traditional Good ol' Texas Brisket"</h3>
		</div>
	</section>

	<section id="content">
		<div class="wrapper">
			<div class="left"></div>
			<div class="right"></div>
		</div>
	</section>

<?php get_footer(); ?>