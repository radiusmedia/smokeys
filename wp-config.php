<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'smokeys');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', '127.0.0.1');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'IauzA8;mI.V#sB_| :.Jma$0;*,SyR!RSi+#ECu$JCgM.1UF4aea#Jf&Su.|fI/]');
define('SECURE_AUTH_KEY',  ')C<F-[$*rz5i?|RR/J,OmfdIZ%KIhY0i=2|JTr7W|[}DJENG|V[F|,^wP?!E7!TD');
define('LOGGED_IN_KEY',    '_$`#~QU0}sx%D:)m iINE5 0,0=k Hh/KAwy <{Gf-]G?m+-Br(QNDvP0(fJ,9_i');
define('NONCE_KEY',        '.V+h<hj}KuECdTv).#a?Nmi0{@g`^w=J3*0}+}.qY=X~dNp<Ie+-:84$c7a:|eQ_');
define('AUTH_SALT',        'y-Zh{8O2M( [^XFswR|U/|6`PA-+B77>0}-aYiPM;+*B~?,r1Fkt%9s)4aBM7LG&');
define('SECURE_AUTH_SALT', 'Q[bx$e_2UGx*zvE0$q~<&[!)F?/J{bjO,z]k7QiR-`*YB5w1Yj7a|3%LqKky%d_f');
define('LOGGED_IN_SALT',   'AUuG8gZ-?fe(m9Ul<V)k{b5hjF sz^~)#[.+%Hm+U$Eg;HLXLw]i4GQk,745Pg6#');
define('NONCE_SALT',       'F~_^cXL,qild]XI6f*F+KLMC#Om)T)L,p:n5RZ}C?2s54;m3X>Oz}i&NDiI}EVN;');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
